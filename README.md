# Splunk Enterprise 8

## SCHEDULE

### Duration
- 3-Days (3 x 8 Hours)
- 5-Half Days (5 x 5 Hours)

### Pre-requisites
- Working exposure to DevOps
- Basic understanding of programming and logging
- Basic understanding of Python and XML

### Lab Setup
- Windows/Mac/Linux with minimum 4GB RAM
- Splunk Enterprise 8 Trial version
- Visual Studio Code or any other text editor
- Curl
- Proxy-free internet
- Sample Data


### Outline

- Introduction
  What is Machine Data?
  Use case for Splunk
  Event
  Fields
  Source
  Sourcetype
  Host
  Timestamp
  Search

- Getting Started with Splunk
  Installing Splunk
  Starting Splunk
  Stopping and Restarting Splunk
  Finding Splunk status

- Exploring Splunk Web
  Administrator: Prefereces and Account Settings
  Activity: Jobs and Alerts
  Users and Authentication: Users, Roles, Authentication
  Server Settings and Controls
  Data Inputs, Inexes and Sourcetypes
  Knowledge Objects

- Hello World
  Add local file data
  Search data using time range
  Understand events
  Create visualization

- Splunk Architecture
  Splunk Daemon
  Web Server
  REST API
  Indexers and Forwarders
  Modular Inputs
  Indexes
  Search Manager and SPL
  Apps

- Search
  Basic search syntax: operators, delimiters, wildcards
  Commands and Functions
  Pipelines
  Subsearches
  Types and Categories of Search
  Search Processing Language
  Search Comamnd Reference
  Using: search, stats, eval, top, rare, fields, sort, chart, timechart, where
  History and Real-time Searces

- Reports
  Understanding Reports
  Creating Reports
  Including visualizations
  Viewing Reports
  Scheduleing Reports
  Accelerating Reports
  Configuring Actions

- Alerts
  Understading Alerts
  Creating Alerts
  Viewing Fired Alerts

- Field Aliases
  Creating and using aliases

- Calculated Fields
  Understading Calculated Fields
  Defining Calculated Fields
  Viewing Calculated Fields
  Using Calculated Fields

- Extracting Fields
  Undestanding Field Extraction
  Extracting using delimiters
  Extracting using regular expressions
  Viewing extracted fields
  Using extracted fields

- Event Types
  Undestanding Event Types
  Defining Event Types
  Using Event Types
  Viewing Event Types

- Tags
  Undestanding Tags
  Assigning Tags to fields
  Assigning Tags to event-types
  Viewing Tags
  Using Tags
   
- Macros
  Understadning Macros
  Defining Macros
  Passing Arguments
  Viewing Macros
  Using Macros

- Workflow Actions
  Undersadning Workflow Actions
  Creating Workflow Actions
  Actions on Fields
  Actions on Events
  Actions to fire a search
  Actions to fire a HTTP GET/POST request
  Passing Arguments
  Viewing Worklow Actions
  Using Workflow Actions

- Transactions
  Understadning Event Correlations
  Group fields
  Group fields and time
  Define transaction macros

- Lookups
  Understanding Lookups
  CSV Lookups
  Create Lookup Tables
  Define Lookups
  Use Lookups
  Automate Lookups

- Visualization
  Understanding Visualization
  Chart Commands
  Generating and viewing Tables
  Charts: Pie, Line, Area, Bar and Column
  Guages, Fillers and Single Value
  Customizing Formats
  Trellis

- Views and Dashboards
  Undersanding Views
  Creating Dashboards
  Adding Events
  Adding Visualizations
  Adding Forms
  Simple XML
  Organizing Panels
  Viewing Dashboards

- App Development
  Understanding Apps
  Apps Vs Add-ons
  Viewing Apps
  Installing apps from splunkbase
  App Directory Structure
  Creating App
  Adding Knowledge Objects
  Adding Views and Dashboards
  Adding Data Inputs
  Inspecting Apps
  Packaging Apps
  Installing Pacakges Apps

- Data Input
  Monitoring Files and Directories
  Script Data Generators
  Modular Data Input Add-on
  Splunkbase
  Monitoring REST Endpoints

- Index Management
  Understanding Indexes
  Raw Files and Index Files
  Buckets
  Rolling Buckets
  Index Types
  Pipelines: Parsing and Indexing
  Default and Internal Indexes
  Creating Indexes
  Using Indexes
  Searching Indexes
  Deleting events from future searches
  Remving data from indexes
  Disabling indexes
  Removing indexes
  REST API

- Job Inspector
  Understanding search execution
  Using Job Inspector
  Interpretting the figures

- REST API
  Understanding REST Concepts
  Splunk REST Endpoints
  Using Curl
  Using Search REST API
  Using Knowledge Object REST API

- Best Practices and Case Study

## REFERENCES

* Splunk Enterprise [https://www.splunk.com/en_us/download/splunk-enterprise.html](https://www.splunk.com/en_us/download/splunk-enterprise.html)
* Sample Data: [http://docs.splunk.com/images/Tutorial/tutorialdata.zip](http://docs.splunk.com/images/Tutorial/tutorialdata.zip)
* Sample Lookup CSV Store: [http://docs.splunk.com/images/d/db/Prices.csv.zip](ttp://docs.splunk.com/images/d/db/Prices.csv.zip)
* Sample REST Input: https://jsonplaceholder.typicode.com/comments
* Videos: [https://drive.google.com/drive/u/0/folders/1YCf4N_Fuw7ysVHFZZFleKri97ANutpuZ](https://drive.google.com/drive/u/0/folders/1YCf4N_Fuw7ysVHFZZFleKri97ANutpuZ)
* Search Mannual: https://docs.splunk.com/Documentation/Splunk/8.2.0/Search/GetstartedwithSearch
* Search Reference: https://docs.splunk.com/Documentation/Splunk/8.2.0/SearchReference/WhatsInThisManual
* Knowledge Objects: https://docs.splunk.com/Documentation/Splunk/8.2.0/Knowledge/WhatisSplunkknowledge
* Visualization and Dashboards: https://docs.splunk.com/Documentation/Splunk/8.2.0/Viz/Aboutthismanual
* Reports: https://docs.splunk.com/Documentation/Splunk/8.2.0/Report/Aboutreports
* Data Input: https://docs.splunk.com/Documentation/Splunk/8.2.0/Data/WhatSplunkcanmonitor
* Index Management: https://docs.splunk.com/Documentation/Splunk/8.2.0/Indexer/Aboutindexesandindexers
* App Development: 
	- https://dev.splunk.com/enterprise/docs/welcome/  
	- https://docs.splunk.com/Documentation/Splunk/8.2.0/AdvancedDev/Whatsinthismanual
* Best Practices: https://docs.splunk.com/Documentation/Splunk/8.2.0/Search/Aboutoptimization
* Splunk Recording: [https://drive.google.com/file/d/1e8ckAUGhzFjsLUjZuy1zkI6BB4Er05dk/view?usp=sharing](https://drive.google.com/file/d/1e8ckAUGhzFjsLUjZuy1zkI6BB4Er05dk/view?usp=sharing)

## VIDEOS 
* 07 JUNE 2021 https://www.dropbox.com/s/1p9shd4h1itcor0/Splunk-Day-1.mp4?dl=0
* 06 JUNE 2021 https://www.dropbox.com/s/8ji7z16170o4zbq/Splunk-Day-2.mp4?dl=0
* 09 JUNE 2021 https://www.dropbox.com/s/c4o18i2wc9xzoxq/Splunk-Day-3.mp4?dl=0
* 10 JUNE 2021 https://www.dropbox.com/s/ny29r2eqq7cdcxv/Splunk-Day-4.mp4?dl=0
* 11 JUNE 2021 https://www.dropbox.com/s/q1g1owkc4w0n8do/Splunk-Day-5.mp4?dl=0

## ILLUSTRATIONS

* EventGen Scripting Data Input Add-on [https://bitbucket.org/glarimy/glarimy-splunk/src/master/spaces/](9https://bitbucket.org/glarimy/glarimy-splunk/src/master/spaces/)
* REST API Input Add-on [https://bitbucket.org/glarimy/glarimy-splunk/src/master/rest/](https://bitbucket.org/glarimy/glarimy-splunk/src/master/rest/)
* App with Dashboards (Simple) [https://bitbucket.org/glarimy/glarimy-splunk/src/master/cospaces/](https://bitbucket.org/glarimy/glarimy-splunk/src/master/cospaces/)
* Add with Dashboards (Inputs) [https://bitbucket.org/glarimy/glarimy-splunk/src/master/rooms/](https://bitbucket.org/glarimy/glarimy-splunk/src/master/rooms/)

## BEST PRACTICES

1. Partition different types of data to separate indexes
2. Specify the index, source and sourcetype in the searches
3. Narrow the time window
4. Be specific. Use key=value. Avoid wildcards. Avoid NOT and !=
5. Limit the number of events. Use `head`
6. Filter early and delay calculations, sorts, stats and etc., 
7. Use projections. Use `fields`
8. Disable field discovery when not required.
9. Avoid event-types, if possible
10. Avoid needless pipelines

### ASSIGNMENTS

Find the product name that is the least preferred.

``sourcetype="access_combined_wcookie" | rare productname limit=1``

Find the total value of the sales from most popular product
``sourcetype="access_combined_wcookie" [search | top productId limit=1 |  fields productId] | stats sum(price)``

Create a pie chart with the top 3 client ip address with whom we did the business
``sourcetype="access_combined_wcookie" | top clientip limit=3``

Schedule a report that contains a bar chart of average sales of various products
``sourcetype="access_combined_wcookie" | chart avg(price) as sales by productId``

Find which client is getting more number of errors
``sourcetype="access_combined_wcookie" status>=400 |  top clientip limit=1``