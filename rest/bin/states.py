from __future__ import print_function

import requests
import time
from datetime import datetime
import random


url = "https://api.data.gov.in/resource/3b01bcb8-0b14-4abf-b6f2-c1bfd384ba69?api-key=579b464db66ec23bdd000001cdd3946e44ce4aad7209ff7b23ac571b&format=csv&offset=0&limit=10"
resp = requests.get(url)
lines = resp.content.splitlines();
lines.pop(0);
for line in lines:
    print(line)
