import sys
import time
from datetime import datetime
import random

for x in range(0, 5):
    event = str(time.time()) + " "
    event += "client=" + random.choice(["\"GreenForce Technologies\"", "\"Richie\"", "\"Sun Lite\""]) + " "
    event += "duration=" + random.choice(["5", "2", "3"]) + " "
    event += "price=" + random.choice(["25000", "50000"]) + " "
    event += "topic=" + random.choice(["Splunk", "ELK", "Dockers", "Dockers", "Kubernetes", "Istio"]) + " "
    event += "online=" + random.choice(["1", "0"]) + " "
    event += "notes=" + random.choice(["\"labs, assessments\"", "\"labs, case-study, certification\"", "\"certification\"", "\"labs, certification\""]) + " "
    print (event)
